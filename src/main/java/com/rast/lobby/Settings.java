package com.rast.lobby;

import com.rast.gamecore.util.ConfigSettings;
import com.rast.gamecore.util.StringLocation;
import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;

import java.util.Objects;

public class Settings extends ConfigSettings {


    private Location spawnLocation;
    private int minimumLevel;

    /**
     * All the settings for the GameLobby or any other plugin that wants to read them
     */
    public Settings() {
        reload();
    }

    public void reload() {
        // Get the plugin, reload config, and get the config
        Lobby plugin = Lobby.getPlugin();
        plugin.reloadConfig();
        FileConfiguration config = plugin.getConfig();
        spawnLocation = StringLocation.toLocation(Objects.requireNonNull(config.getString("spawn-location")));
        minimumLevel = config.getInt("minimumlevel", 0);
    }

    /**
     * Get the spawn location of the lobby
     *
     * @return the spawn location
     */
    public Location getSpawnLocation() {
        return spawnLocation;
    }

    /**
     * Get the minimum y-level that the player can reach before being teleported back to spawn
     *
     * @return the minimum y-level
     */
    public int getMinimumLevel() {
        return minimumLevel;
    }
}
