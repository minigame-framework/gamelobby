package com.rast.lobby;

import com.rast.gamecore.GameCore;
import com.rast.gamecore.util.CommandManager;
import com.rast.gamecore.util.Subcommand;
import com.rast.lobby.events.MenuEvents;
import com.rast.lobby.events.PlayerHealing;
import com.rast.lobby.events.Respawning;
import com.rast.lobby.events.WorldGuard;
import com.rast.lobby.gui.GameMenuManager;
import com.rast.lobby.gui.HotbarGui;
import com.rast.lobby.subcommand.HelpSubcommand;
import com.rast.lobby.subcommand.SetSpawnSubcommand;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffect;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

public class Lobby extends JavaPlugin {

    private static Lobby plugin;
    private static Settings settings;
    private static LobbyGame lobby;
    private static GameMenuManager gameMenuManager;
    private static HotbarGui hotbarGui;
    private static final HashMap<String, Subcommand> subCommands = new HashMap<>();

    /**
     * Get the GameLobby plugin
     *
     * @return the GameLobby plugin
     */
    public static Lobby getPlugin() {
        return plugin;
    }

    /**
     * Get the Lobby object
     * The lobby class extends to Game of GameCore
     *
     * @return the Lobby
     */
    public static LobbyGame getLobby() {
        return lobby;
    }

    /**
     * Get the settings for GameLobby
     *
     * @return game lobby settings
     */
    public static Settings getSettings() {
        return settings;
    }

    /**
     * Get a hashmap of subcommands for GameLobby
     *
     * @return game lobby subcommands in a hashmap
     */
    public static HashMap<String, Subcommand> getSubCommands() {
        return subCommands;
    }

    /**
     * Send the player to the lobby spawn and set up their inventory
     *
     * @param player the player to teleport
     */
    public static void sendToSpawn(Player player) {
        for (PotionEffect effect : player.getActivePotionEffects()) {
            player.removePotionEffect(effect.getType());
        }
        player.setHealth(20);
        player.setFoodLevel(20);
        player.teleport(Lobby.getSettings().getSpawnLocation());
        player.setFallDistance(0);
        player.setFireTicks(0);
    }

    /**
     * Get the GameMenuManager
     *
     * @return the GameMenuManager
     */
    public static GameMenuManager getGameMenuManager() {
        return gameMenuManager;
    }

    /**
     * Get the HotbarGui
     * The hotbarGui remains static for all players in the lobby
     *
     * @return the HotbarGui
     */
    public static HotbarGui getHotbarGui() {
        return hotbarGui;
    }

    @Override
    public void onEnable() {
        plugin = this;
        saveDefaultConfig();

        // Load settings
        settings = new Settings();

        // Get new instances
        lobby = new LobbyGame("Lobby", new ArrayList<>(), this);
        gameMenuManager = new GameMenuManager();
        hotbarGui = new HotbarGui(null);

        // Generate configs if they are not generated
        File configFile;
        configFile = new File(plugin.getDataFolder().getPath() + "/menus/gamelistmenu.yml");

        if (!configFile.exists()) {
            configFile.getParentFile().mkdir();
            plugin.saveResource("menus/gamelistmenu.yml", true);
        }

        configFile = new File(plugin.getDataFolder().getPath() + "/menus/gamemenu.yml");

        if (!configFile.exists()) {
            configFile.getParentFile().mkdir();
            plugin.saveResource("menus/gamemenu.yml", true);
        }

        configFile = new File(plugin.getDataFolder().getPath() + "/menus/hotbar.yml");

        if (!configFile.exists()) {
            configFile.getParentFile().mkdir();
            plugin.saveResource("menus/hotbar.yml", true);
        }

        configFile = new File(plugin.getDataFolder().getPath() + "/menus/maplistmenu.yml");

        if (!configFile.exists()) {
            configFile.getParentFile().mkdir();
            plugin.saveResource("menus/maplistmenu.yml", true);
        }

        configFile = new File(plugin.getDataFolder().getPath() + "/menus/worldlistmenu.yml");

        if (!configFile.exists()) {
            configFile.getParentFile().mkdir();
            plugin.saveResource("menus/worldlistmenu.yml", true);
        }

        // Register with gamecore
        GameCore.getGameMaster().registerGame(lobby);
        GameCore.getGameMaster().createPlayerGroup(lobby.getName());

        // Register events
        getServer().getPluginManager().registerEvents(new Respawning(), this);
        getServer().getPluginManager().registerEvents(new WorldGuard(), this);
        getServer().getPluginManager().registerEvents(new PlayerHealing(), this);
        getServer().getPluginManager().registerEvents(new MenuEvents(), this);

        // Set up commands for this plugin
        subCommands.put("setspawn", new SetSpawnSubcommand(this));
        subCommands.put("help", new HelpSubcommand(this));
        CommandManager commandManager = new CommandManager(this, subCommands);
        Objects.requireNonNull(getCommand("gamelobby")).setExecutor(commandManager);
    }
}
