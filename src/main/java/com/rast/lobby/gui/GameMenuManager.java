package com.rast.lobby.gui;

import org.bukkit.entity.Player;

import java.util.HashMap;

public class GameMenuManager {

    private final HashMap<Player, GameMenu> playerMenus = new HashMap<>();

    /**
     * Get the GameMenu from the player
     *
     * @param player the player to get the GameMenu of
     * @return the GameMenu
     */
    public GameMenu getGameMenu(Player player) {
        return playerMenus.get(player);
    }

    /**
     * Set the player's GameMenu
     *
     * @param player   the player to set the GameMenu to
     * @param gameMenu the GameMenu to set to the player
     */
    public void setGameMenu(Player player, GameMenu gameMenu) {
        playerMenus.put(player, gameMenu);
    }

}
