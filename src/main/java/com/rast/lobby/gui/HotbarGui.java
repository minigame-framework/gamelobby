package com.rast.lobby.gui;

import com.rast.gamecore.Game;
import com.rast.gamecore.util.ColorText;
import com.rast.lobby.Lobby;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.inventory.Inventory;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class HotbarGui extends GameMenu {

    private List<MenuButton> menuButtons;
    private Inventory inventory;
    private Game game;

    public HotbarGui(Game game) {
        super(game);
    }

    @Override
    public void generateGameMenu(Game game) {
        this.game = game;
        Lobby plugin = Lobby.getPlugin();

        // Get the menu file config
        File configFile = new File(plugin.getDataFolder().getPath() + "/menus/hotbar.yml");

        // Load the config
        FileConfiguration menuConfig = YamlConfiguration.loadConfiguration(configFile);

        // Get the size, title of the menu
        int size = 9;
        String title = "hotbar";

        // Create the new inventory
        Inventory menu = Bukkit.createInventory(null, size, title);

        // Reset lists
        menuButtons = new ArrayList<>();

        // Add each item in the inventory
        for (int i = 0; i < size; i++) {

            // Get the config for the item slot
            ConfigurationSection slotConfig = menuConfig.getConfigurationSection("slot-" + i);

            // If the slot config does not exist, continue
            if (slotConfig == null) {
                continue;
            }

            Material itemMaterial = Material.getMaterial(Objects.requireNonNull(slotConfig.getString("item")));
            String itemName = ColorText.TranslateChat(Objects.requireNonNull(slotConfig.getString("name")));
            List<String> itemLore = slotConfig.getStringList("lore");
            boolean itemEnchanted = slotConfig.getBoolean("enchanted", false);
            ButtonFunction itemFunction = ButtonFunction.valueOf(Objects.requireNonNull(slotConfig.getString("function")));
            List<?> itemData = slotConfig.getList("data");

            MenuButton button = new MenuButton(itemMaterial, itemName, itemLore, itemFunction, itemData, itemEnchanted);
            menuButtons.add(button);
            menu.setItem(i, button.getButtonItem());
        }
        inventory = menu;
    }

    /**
     * Get the inventory for this GameMenu
     *
     * @return the game menu inventory
     */
    @Override
    public Inventory getInventory() {
        return inventory;
    }

    /**
     * Get a list of the game menu functions sorted by slot
     *
     * @return the menu functions
     */
    @Override
    public List<MenuButton> getMenuButtons() {
        return menuButtons;
    }

    /**
     * Get the game that this menu is for
     *
     * @return the game the menu is for
     */
    @Override
    public Game getGame() {
        return game;
    }
}