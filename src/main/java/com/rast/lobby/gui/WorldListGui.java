package com.rast.lobby.gui;

import com.rast.gamecore.Game;
import com.rast.gamecore.GameWorld;
import com.rast.gamecore.util.ColorText;
import com.rast.lobby.Lobby;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.inventory.Inventory;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class WorldListGui extends GameMenu {

    private List<MenuButton> menuButtons;
    private Inventory inventory;
    private Game game;

    public WorldListGui(Game game) {
        super(game);
    }

    @Override
    public void generateGameMenu(Game game) {
        this.game = game;
        Lobby plugin = Lobby.getPlugin();

        // Get the menu file config
        File configFile = new File(plugin.getDataFolder().getPath() + "/menus/worldlistmenu.yml");

        // Load the config
        FileConfiguration menuConfig = YamlConfiguration.loadConfiguration(configFile);

        // Get the worlds
        List<GameWorld> gameWorlds;
        if (menuConfig.getBoolean("open-only", false)) {
            gameWorlds = game.getSortedOpenGameList();
        } else {
            gameWorlds = game.getSortedGameList();
        }

        // Get the size, title of the menu
        int size = gameWorlds.size() + 1;
        if (size % 9 != 0) {
            size = size - size % 9 + 9;
        }
        size = Math.max(9, Math.min(54, size));

        String title = ColorText.TranslateChat(Objects.requireNonNull(menuConfig.getString("menu-title", " ")));
        title = title.replace("%game%", game.getName());

        // Create the new inventory
        Inventory menu = Bukkit.createInventory(null, size, title);

        // Reset lists
        menuButtons = new ArrayList<>();

        Material woMaterial = Material.getMaterial(Objects.requireNonNull(menuConfig.getString("waiting-open-item")));
        Material wcMaterial = Material.getMaterial(Objects.requireNonNull(menuConfig.getString("waiting-closed-item")));
        Material roMaterial = Material.getMaterial(Objects.requireNonNull(menuConfig.getString("running-open-item")));
        Material rcMaterial = Material.getMaterial(Objects.requireNonNull(menuConfig.getString("running-closed-item")));
        String itemName = ColorText.TranslateChat((Objects.requireNonNull(menuConfig.getString("world-name"))));
        List<String> itemLore = menuConfig.getStringList("world-lore");
        boolean itemEnchanted = menuConfig.getBoolean("enchanted", false);

        for (GameWorld world : gameWorlds) {
            Material material;
            switch (world.getStatus()) {
                case WAITING_OPEN:
                    material = woMaterial;
                    break;
                case WAITING_CLOSED:
                    material = wcMaterial;
                    break;
                case RUNNING_OPEN:
                    material = roMaterial;
                    break;
                case RUNNING_CLOSED:
                    material = rcMaterial;
                    break;
                default:
                    material = Material.BARRIER;
            }

            String newItemName = itemName.replace("%map%", world.getMap());
            newItemName = newItemName.replace("%count%", String.valueOf(world.getPlayerCount()));
            newItemName = newItemName.replace("%max%", String.valueOf(world.getMaxPlayerCount()));
            List<String> tmpLore = new ArrayList<>();
            for (String loreline : itemLore) {
                tmpLore.add(loreline.replace("%status%", world.getStatus().toString().replace('_', ' ')));
            }

            MenuButton button = new MenuButton(material, newItemName, tmpLore, ButtonFunction.PLAY_GAME_WORLD, new ArrayList<>(Collections.singletonList(world)), itemEnchanted);
            menuButtons.add(button);
        }

        MenuButton arrowButton = new MenuButton(Material.ARROW, ChatColor.GRAY + "Back", null, ButtonFunction.BACK, new ArrayList<>(), false);
        menuButtons.add(arrowButton);

        // Add each item in the inventory
        for (int i = 0; i < menuButtons.size() - 1; i++) {
            menu.setItem(i, menuButtons.get(i).getButtonItem());
        }
        menu.setItem(size - 1, arrowButton.getButtonItem());
        inventory = menu;
    }

    /**
     * Get the inventory for this GameMenu
     *
     * @return the game menu inventory
     */
    @Override
    public Inventory getInventory() {
        return inventory;
    }

    /**
     * Get a list of the game menu functions sorted by slot
     *
     * @return the menu functions
     */
    @Override
    public List<MenuButton> getMenuButtons() {
        return menuButtons;
    }

    /**
     * Get the game that this menu is for
     *
     * @return the game the menu is for
     */
    @Override
    public Game getGame() {
        return game;
    }
}