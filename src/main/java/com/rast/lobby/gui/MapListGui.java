package com.rast.lobby.gui;

import com.rast.gamecore.Game;
import com.rast.gamecore.util.ColorText;
import com.rast.lobby.Lobby;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.inventory.Inventory;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class MapListGui extends GameMenu {

    private List<MenuButton> menuButtons;
    private Inventory inventory;
    private Game game;

    public MapListGui(Game game) {
        super(game);
    }

    @Override
    public void generateGameMenu(Game game) {
        this.game = game;
        Lobby plugin = Lobby.getPlugin();

        // Get the menu file config
        File configFile = new File(plugin.getDataFolder().getPath() + "/menus/maplistmenu.yml");

        // Load the config
        FileConfiguration menuConfig = YamlConfiguration.loadConfiguration(configFile);

        // Get the size, title of the menu
        int size = game.getMapSet().size();
        size = Math.min(54, Math.max(9, size));
        String title = ColorText.TranslateChat(Objects.requireNonNull(menuConfig.getString("menu-title", " ")));
        title = title.replace("%game%", game.getName());

        // Create the new inventory
        Inventory menu = Bukkit.createInventory(null, size, title);

        // Reset lists
        menuButtons = new ArrayList<>();

        Material itemMaterial = Material.getMaterial(Objects.requireNonNull(menuConfig.getString("map-item")));
        String itemName = ColorText.TranslateChat((Objects.requireNonNull(menuConfig.getString("map-name"))));
        List<String> itemLore = menuConfig.getStringList("map-lore");
        boolean itemEnchanted = menuConfig.getBoolean("enchanted", false);

        if (game.getMapSet() != null) {
            for (String map : game.getMapSet()) {
                MenuButton button = new MenuButton(itemMaterial, itemName.replace("%map%", map), itemLore, ButtonFunction.PLAY_GAME_MAP, new ArrayList<>(Collections.singletonList(map)), itemEnchanted);
                menuButtons.add(button);
            }
        }

        MenuButton arrowButton = new MenuButton(Material.ARROW, ChatColor.GRAY + "Back", null, ButtonFunction.BACK, new ArrayList<>(), false);
        menuButtons.add(arrowButton);

        // Add each item in the inventory
        for (int i = 0; i < menuButtons.size() - 1; i++) {
            menu.setItem(i, menuButtons.get(i).getButtonItem());
        }
        menu.setItem(size - 1, arrowButton.getButtonItem());
        inventory = menu;
    }

    /**
     * Get the inventory for this GameMenu
     *
     * @return the game menu inventory
     */
    @Override
    public Inventory getInventory() {
        return inventory;
    }

    /**
     * Get a list of the game menu functions sorted by slot
     *
     * @return the menu functions
     */
    @Override
    public List<MenuButton> getMenuButtons() {
        return menuButtons;
    }

    /**
     * Get the game that this menu is for
     *
     * @return the game the menu is for
     */
    @Override
    public Game getGame() {
        return game;
    }
}