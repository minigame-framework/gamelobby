package com.rast.lobby.gui;

import com.rast.gamecore.GameCore;
import com.rast.gamecore.util.ColorText;
import com.rast.lobby.Lobby;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.persistence.PersistentDataType;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

public class MenuButton {
    private final ItemStack buttonItem;
    private final ButtonFunction function;
    private final List<?> data;
    private final NamespacedKey uidKey = new NamespacedKey(Lobby.getPlugin(), "buttonID");
    private final String uid = UUID.randomUUID().toString();


    public MenuButton(Material material, String name, List<String> lore, ButtonFunction function, List<?> data, boolean enchanted) {
        ItemStack itemStack = new ItemStack(material);
        ItemMeta itemMeta = itemStack.getItemMeta();
        if (itemMeta != null) {
            if (lore != null) {
                List<String> tmpLore = new ArrayList<>();
                for (String loreline : lore) {
                    tmpLore.add(ColorText.TranslateChat(loreline));
                }
                lore = tmpLore;
            } else {
                lore = new ArrayList<>();
            }
            itemMeta.setDisplayName(name);
            itemMeta.setLore(lore);
            itemMeta.getPersistentDataContainer().set(uidKey, PersistentDataType.STRING, uid);
            if (enchanted) {
                itemMeta.addEnchant(Objects.requireNonNull(Enchantment.getByKey(new NamespacedKey(GameCore.getPlugin(), "glow"))), 1, true);
            }
        }
        itemStack.setItemMeta(itemMeta);
        this.function = function;
        this.data = data;
        buttonItem = itemStack;
    }

    public ItemStack getButtonItem() {
        return buttonItem;
    }

    public ButtonFunction getFunction() {
        return function;
    }

    public List<?> getData() {
        return data;
    }

    public NamespacedKey getUidKey() {
        return uidKey;
    }

    public String getUid() {
        return uid;
    }
}
