package com.rast.lobby.gui;

import com.rast.gamecore.Game;
import org.bukkit.inventory.Inventory;

import java.util.List;

public abstract class GameMenu {

    public GameMenu(Game game) {
        generateGameMenu(game);
    }

    public abstract void generateGameMenu(Game game);

    public abstract Inventory getInventory();

    public abstract List<MenuButton> getMenuButtons();

    public abstract Game getGame();

}
