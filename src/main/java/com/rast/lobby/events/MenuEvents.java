package com.rast.lobby.events;

import com.rast.gamecore.Game;
import com.rast.gamecore.GameCore;
import com.rast.gamecore.GameWorld;
import com.rast.gamecore.util.ColorText;
import com.rast.gamecore.util.EventProxy;
import com.rast.gamecore.util.StringLocation;
import com.rast.lobby.Lobby;
import com.rast.lobby.gui.*;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataType;

import java.util.List;
import java.util.Objects;

public class MenuEvents extends EventProxy implements Listener {

    @EventHandler
    public void onInventoryClick(InventoryClickEvent e) {

        Player player = (Player) e.getWhoClicked();

        if (isValid(player, Lobby.getLobby().getName())) {

            e.setCancelled(true);
            GameMenu menu = Lobby.getGameMenuManager().getGameMenu(player);
            if (menu == null) {
                return;
            }
            List<MenuButton> menuButtons = menu.getMenuButtons();
            MenuButton menuButton = null;
            ItemStack currentItem = e.getCurrentItem();
            if (currentItem == null) {
                return;
            }
            ItemMeta currentMeta = currentItem.getItemMeta();
            if (currentMeta == null) {
                return;
            }
            PersistentDataContainer container = currentMeta.getPersistentDataContainer();
            for (MenuButton button : menuButtons) {
                if (Objects.equals(container.get(button.getUidKey(), PersistentDataType.STRING), button.getUid())) {
                    menuButton = button;
                    break;
                }
            }

            // Initiate game
            Game game;

            if (menuButton == null) {
                return;
            }

            // Figure out what this item does
            switch (menuButton.getFunction()) {
                case GAME_LIST:
                    GameListGui gameListGui = new GameListGui(null);
                    Lobby.getGameMenuManager().setGameMenu(player, gameListGui);
                    Bukkit.getServer().getScheduler().runTask(Lobby.getPlugin(), () -> player.openInventory(gameListGui.getInventory()));
                    break;
                case INFO:
                    // print the text
                    for (Object object : menuButton.getData()) {
                        player.sendMessage(ColorText.TranslateChat((String) object));
                    }
                    break;
                case TELEPORT:
                    player.teleport(StringLocation.toLocation((String) menuButton.getData().get(0)));
                    break;
                case GAME_MENU:
                    game = GameCore.getGameMaster().getGame((String) menuButton.getData().get(0));
                    if (game == null) {
                        player.closeInventory();
                        player.sendMessage(GameCore.getLocale().getSystemPrefix() + GameCore.getLocale().getErrorNoGame());
                        return;
                    }
                    GameMenu gameGui = new GameGui(game);
                    Lobby.getGameMenuManager().setGameMenu(player, gameGui);
                    Bukkit.getServer().getScheduler().runTask(Lobby.getPlugin(), () -> player.openInventory(gameGui.getInventory()));
                    break;
                case PLAY_GAME:
                    game = menu.getGame();
                    if (game != null) {
                        player.closeInventory();
                        GameCore.getGameMaster().getPlayerGame(player).pullPlayer(player);
                        game.sendPlayer(player);
                    } else {
                        player.sendMessage(GameCore.getLocale().getSystemPrefix() + GameCore.getLocale().getErrorNoGame());
                    }
                    break;
                case PLAY_GAME_MAP:
                    game = menu.getGame();
                    if (game != null) {
                        if (game.getMapSet().contains((String.valueOf(menuButton.getData().get(0))))) {
                            player.closeInventory();
                            GameCore.getGameMaster().getPlayerGame(player).pullPlayer(player);
                            game.sendPlayer(player, (String) menuButton.getData().get(0));
                        } else {
                            player.sendMessage(GameCore.getLocale().getSystemPrefix() + GameCore.getLocale().getErrorNoMap());
                        }
                    } else {
                        player.sendMessage(GameCore.getLocale().getSystemPrefix() + GameCore.getLocale().getErrorNoGame());
                    }
                    break;
                case PLAY_GAME_WORLD:
                    game = menu.getGame();
                    if (game != null) {
                        player.closeInventory();
                        GameCore.getGameMaster().getPlayerGame(player).pullPlayer(player);
                        if (e.getAction() == InventoryAction.PICKUP_HALF) {
                            game.sendSpectator(player, (GameWorld) menuButton.getData().get(0));
                            break;
                        }
                        game.sendPlayer(player, (GameWorld) menuButton.getData().get(0));
                    } else {
                        player.sendMessage(GameCore.getLocale().getSystemPrefix() + GameCore.getLocale().getErrorNoGame());
                    }
                    break;
                case MAP_MENU:
                    game = menu.getGame();
                    if (game == null) {
                        player.closeInventory();
                        player.sendMessage(GameCore.getLocale().getSystemPrefix() + GameCore.getLocale().getErrorNoGame());
                        return;
                    }
                    MapListGui mapListGui = new MapListGui(game);
                    Lobby.getGameMenuManager().setGameMenu(player, mapListGui);
                    Bukkit.getServer().getScheduler().runTask(Lobby.getPlugin(), () -> player.openInventory(mapListGui.getInventory()));
                    break;
                case WORLD_MENU:
                    game = menu.getGame();
                    if (game == null) {
                        player.closeInventory();
                        player.sendMessage(GameCore.getLocale().getSystemPrefix() + GameCore.getLocale().getErrorNoGame());
                        return;
                    }
                    WorldListGui worldListGui = new WorldListGui(game);
                    Lobby.getGameMenuManager().setGameMenu(player, worldListGui);
                    Bukkit.getServer().getScheduler().runTask(Lobby.getPlugin(), () -> player.openInventory(worldListGui.getInventory()));
                    break;
                case BACK:
                    game = menu.getGame();
                    if (menu instanceof WorldListGui || menu instanceof MapListGui) {
                        if (game == null) {
                            player.closeInventory();
                            player.sendMessage(GameCore.getLocale().getSystemPrefix() + GameCore.getLocale().getErrorNoGame());
                            return;
                        }
                        GameMenu gameGUI = new GameGui(game);
                        Lobby.getGameMenuManager().setGameMenu(player, gameGUI);
                        Bukkit.getServer().getScheduler().runTask(Lobby.getPlugin(), () -> player.openInventory(gameGUI.getInventory()));
                    } else if (menu instanceof GameGui) {
                        GameMenu gameListGui2 = new GameListGui(null);
                        Lobby.getGameMenuManager().setGameMenu(player, gameListGui2);
                        Bukkit.getServer().getScheduler().runTask(Lobby.getPlugin(), () -> player.openInventory(gameListGui2.getInventory()));
                        break;
                    } else if (menu instanceof GameListGui) {
                        player.closeInventory();
                    }
            }

        }
    }

    @EventHandler
    public void onItemThrow(PlayerDropItemEvent e) {
        if (isValid(e.getPlayer(), Lobby.getLobby().getName())) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent e) {
        Player player = e.getPlayer();
        if (isValid(player, Lobby.getLobby().getName())) {

            e.setCancelled(true);

            // Get the hotbar data
            HotbarGui hotbar = Lobby.getHotbarGui();
            if (hotbar == null) {
                return;
            }
            List<MenuButton> menuButtons = hotbar.getMenuButtons();
            MenuButton menuButton = null;
            if (!e.hasItem()) {
                return;
            }
            PersistentDataContainer container = Objects.requireNonNull(Objects.requireNonNull(e.getItem()).getItemMeta()).getPersistentDataContainer();
            for (MenuButton button : menuButtons) {
                if (Objects.equals(container.get(button.getUidKey(), PersistentDataType.STRING), button.getUid())) {
                    menuButton = button;
                    break;
                }
            }

            if (menuButton == null) {
                return;
            }

            // figure out what this item does
            switch (menuButton.getFunction()) {
                case GAME_LIST:
                    GameListGui gameListGui = new GameListGui(null);
                    Lobby.getGameMenuManager().setGameMenu(player, gameListGui);
                    player.openInventory(gameListGui.getInventory());
                    break;
                case INFO:
                    // print the text
                    for (Object object : menuButton.getData()) {
                        player.sendMessage(ColorText.TranslateChat((String) object));
                    }
                    break;
                case TELEPORT:
                    player.teleport(StringLocation.toLocation((String) menuButton.getData().get(0)));
                    break;
            }
        }
    }
}
