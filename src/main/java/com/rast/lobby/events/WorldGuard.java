package com.rast.lobby.events;

import com.rast.gamecore.GameCore;
import com.rast.gamecore.util.EventProxy;
import com.rast.lobby.Lobby;
import org.bukkit.block.Container;
import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.hanging.HangingBreakEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;

public class WorldGuard extends EventProxy implements Listener {

    @EventHandler
    public void OnBlockPlace(BlockPlaceEvent e) {
        if (isValid(e.getPlayer(), Lobby.getLobby().getName()) && !e.getPlayer().hasPermission("gamelobby.build")) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void OnBlockBreak(BlockBreakEvent e) {
        if (isValid(e.getPlayer(), Lobby.getLobby().getName()) && !e.getPlayer().hasPermission("gamelobby.build")) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onHangingBreak(HangingBreakEvent event) {
        if (isValid(event.getEntity(), GameCore.getSettings().getDefaultWorld())) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onDamageEntity(EntityDamageEvent event) {
        if (isValid(event.getEntity(), GameCore.getSettings().getDefaultWorld())) {
            if (event.getEntityType().equals(EntityType.ITEM_FRAME)) {
                event.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void onPlayerInteractEntity(PlayerInteractEntityEvent event) {
        if (isValid(event.getPlayer(), Lobby.getLobby().getName())) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void OnPlayerInteract(PlayerInteractEvent e) {
        if (isValid(e.getPlayer(), Lobby.getLobby().getName()) && !(e.getPlayer().hasPermission("gamelobby.use") || e.getPlayer().hasPermission("gamelobby.build"))) {
            if (e.getClickedBlock() != null) {
                if (e.getClickedBlock().getState() instanceof Container) {
                    e.setCancelled(true);
                }
            }
        }
    }

}
