package com.rast.lobby.events;

import com.rast.gamecore.GameCore;
import com.rast.gamecore.util.EventProxy;
import com.rast.lobby.Lobby;
import org.bukkit.World;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerMoveEvent;

import java.util.Objects;

public class Respawning extends EventProxy implements Listener {

    private static final World defaultWorld = GameCore.getSettings().getDefaultWorld();

    @EventHandler
    public void OnPlayerDeath(PlayerDeathEvent e) {
        if (isValid(e.getEntity(), defaultWorld, Lobby.getLobby().getName())) {
            Lobby.sendToSpawn(e.getEntity());
        }
    }

    @EventHandler
    public void OnPlayerMove(PlayerMoveEvent e) {
        if (isValid(e.getPlayer(), defaultWorld, Lobby.getLobby().getName())) {
            if (Objects.requireNonNull(e.getTo()).getY() < Lobby.getSettings().getMinimumLevel()) {
                Lobby.sendToSpawn(e.getPlayer());
            }
        }
    }
}
