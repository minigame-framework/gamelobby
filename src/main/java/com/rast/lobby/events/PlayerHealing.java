package com.rast.lobby.events;

import com.rast.gamecore.GameCore;
import com.rast.gamecore.util.EventProxy;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;

public class PlayerHealing extends EventProxy implements Listener {

    private static final World defaultWorld = GameCore.getSettings().getDefaultWorld();

    @EventHandler
    public void onEntityDamage(EntityDamageEvent e) {
        if (isValid(e.getEntity(), defaultWorld) && e.getEntity() instanceof Player) {
            e.getEntity().setFireTicks(0);
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onPlayerHunger(FoodLevelChangeEvent e) {
        if (isValid(e.getEntity(), defaultWorld) && e.getEntity() instanceof Player) {
            e.setFoodLevel(20);
            e.setCancelled(true);
        }
    }
}
