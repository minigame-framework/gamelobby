package com.rast.lobby.subcommand;

import com.rast.gamecore.util.Subcommand;
import com.rast.lobby.Lobby;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;

public class HelpSubcommand extends Subcommand {


    public HelpSubcommand(Lobby plugin) {
        super(plugin);
    }

    @Override
    public String getHelp() {
        return "Gives help for the plugin";
    }

    @Override
    public String getUsage(String label) {
        return "/" + label + " help";
    }

    @Override
    public String getPermission() {
        return "gamelobby.admin";
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player))
            return false;

        final Player player = (Player) sender;

        player.sendMessage(" ");
        player.sendMessage(ChatColor.YELLOW + "Help for game lobby:");

        for (Subcommand subcommand : Lobby.getSubCommands().values()) {
            if (player.hasPermission(subcommand.getPermission()))
                player.sendMessage(ChatColor.GOLD + subcommand.getUsage(label) + " : " + subcommand.getHelp());
        }
        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
        return null;
    }
}
