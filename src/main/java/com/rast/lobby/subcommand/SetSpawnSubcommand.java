package com.rast.lobby.subcommand;

import com.rast.gamecore.GameCore;
import com.rast.gamecore.util.StringLocation;
import com.rast.gamecore.util.Subcommand;
import com.rast.lobby.Lobby;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import java.util.List;

public class SetSpawnSubcommand extends Subcommand {


    public SetSpawnSubcommand(Lobby plugin) {
        super(plugin);
    }

    @Override
    public String getHelp() {
        return "Sets the spawn location for the lobby";
    }

    @Override
    public String getUsage(String label) {
        return "/" + label + " setspawn";
    }

    @Override
    public String getPermission() {
        return "gamelobby.admin";
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player))
            return false;

        final Player player = (Player) sender;
        Location loc = player.getLocation();
        FileConfiguration config = plugin.getConfig();
        config.set("spawn-location", StringLocation.toString(loc));
        plugin.saveConfig();
        Lobby.getSettings().reload();
        player.sendMessage(GameCore.getLocale().getSystemPrefix() + ChatColor.GOLD + "Set lobby spawn point to your current location.");
        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
        return null;
    }
}