package com.rast.lobby;

import com.rast.gamecore.*;
import com.rast.gamecore.util.CleanPlayer;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class LobbyGame extends Game {

    public LobbyGame(String name, List<String> mapList, JavaPlugin plugin) {
        super(name, mapList, plugin);
    }

    @Override
    public void sendPlayer(Player player, GameWorld gameWorld) {
        sendPlayer(player);
    }

    /**
     * Send the player to the lobby
     * Once the player is sent, the lobby becomes responsible for the player
     *
     * @param player the player to send
     * @param map    the map is not used for the game lobby
     */
    @Override
    public void sendPlayer(Player player, String map) {
        sendPlayer(player);
    }

    /**
     * Send the player to the lobby
     * Once the player is sent, the lobby becomes responsible for the player
     *
     * @param player the player to send
     */
    @Override
    public void sendPlayer(Player player) {
        GameMaster gameMaster = GameCore.getGameMaster();
        gameMaster.addToPlayerGroup(getName(), player);
        gameMaster.setPlayerGame(player, this);
        getPlayerSet().add(player);
        Lobby.sendToSpawn(player);
        CleanPlayer.cleanAll(player);
        player.getInventory().setContents(Lobby.getHotbarGui().getInventory().getContents());
        player.setGameMode(GameMode.ADVENTURE);
    }

    @Override
    public void sendSpectator(Player player, GameWorld gameWorld) {
        sendPlayer(player);
    }

    /**
     * Take the player out of the lobby
     *
     * @param player the player to remove
     */
    @Override
    public void pullPlayer(Player player) {
        GameMaster gameMaster = GameCore.getGameMaster();
        getPlayerSet().remove(player);
        gameMaster.removePlayerFromGroup(getName(), player);
        gameMaster.removePlayerGame(player);
        CleanPlayer.cleanAll(player);
    }

    @Override
    public String getChatFormat(Player player) {
        return GameCore.getSettings().getGlobalChatFormat();
    }

    @Override
    public GameInstance getNewGameInstance(GameWorld gameWorld) {
        return null;
    }

    /**
     * Get a set of maps for this game
     *
     * @return an empty hashset
     */
    @Override
    public Set<String> getMapSet() {
        return new HashSet<>();
    }

    /**
     * Get a set of game worlds for this game
     *
     * @return an empty list
     */
    @Override
    public Set<GameWorld> getGameSet() {
        return new HashSet<>();
    }

    /**
     * Get a sorted list of maps for this game in alphabetical order
     *
     * @return an empty list
     */
    @Override
    public List<String> getSortedMapList() {
        return new ArrayList<>();
    }
}